create table product (
    id serial primary key,
    name varchar(50) not null,
    description varchar,
    price numeric(12,2) not null,
    count integer
);

create table account (
    id serial primary key,
    first_name varchar(50) not null,
    last_name  varchar(50) not null,
    email varchar(250) unique not null,
    password varchar(250) not null
);



/*
--drop table product
--drop table account

insert into product (name, description, price, count)
values ('CPU', 'Процессор Intel', '800', '10');

insert into product (name, description, price, count)
values ('HDD', 'Жесткий диск Kingston', '200', '40');

insert into product (name, description, price, count)
values ('SSD', 'Твердотельный накопитель Transcend', '300', '35');

insert into product (name, description, price, count)
values ('Motherboard', 'Материнская плата Asus', '500', '15');*/
