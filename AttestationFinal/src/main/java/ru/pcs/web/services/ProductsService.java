package ru.pcs.web.services;

import ru.pcs.web.forms.ProductForm;
import ru.pcs.web.models.Product;

import java.util.List;

public interface ProductsService {
    List<Product> getAllProduct();
    Product getProduct(Integer productId);

    void addProduct(ProductForm form);
    void deleteProduct(Integer productId);
    void updateProduct(Integer productId, ProductForm form);
}
