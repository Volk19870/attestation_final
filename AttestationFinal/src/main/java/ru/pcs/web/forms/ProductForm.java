package ru.pcs.web.forms;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.math.BigDecimal;

@Data
public class ProductForm {
    @Length(max = 50)
    private String name;
    private String description;
    private BigDecimal price;
    private Integer count;
}
