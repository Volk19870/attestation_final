package ru.pcs.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.pcs.web.forms.ProductForm;
import ru.pcs.web.models.Product;
import ru.pcs.web.services.ProductsService;

import java.util.List;

@Controller
public class ProductsController {
    private final ProductsService productsService;

    @Autowired
    public ProductsController(ProductsService productsService) {
        this.productsService = productsService;
    }

    @GetMapping("/products")
    public String getProductsPage(Model model) {
        List<Product> products = productsService.getAllProduct();
        model.addAttribute("products", products);
        return "products";
    }

    @GetMapping("/products/{product-id}")
    public String getProductPage(@PathVariable("product-id") Integer productId, Model model) {
        Product product = productsService.getProduct(productId);
        model.addAttribute("product", product);
        return "product";
    }

    @GetMapping("/products/add")
    public String getProductPageAdd() {
        return "product_add";
    }

    @PostMapping("/products/add")
    public String addProduct(ProductForm form) {
        productsService.addProduct(form);
        return "redirect:/products";
    }

    @PostMapping("/products/{product-id}/delete")
    public String deleteProduct(@PathVariable("product-id") Integer productId) {
        productsService.deleteProduct(productId);
        return "redirect:/products";
    }

    @PostMapping("/products/{product-id}/update")
    public String updateProduct(@PathVariable("product-id") Integer productId, ProductForm form) {
        productsService.updateProduct(productId, form);
        return "redirect:/products";
    }
}
